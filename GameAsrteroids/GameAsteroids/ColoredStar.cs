﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace GameAsteroids
{
    public class ColoredStar : Star
    {
        public ColoredStar(Point pos, Point dir, Size size, Pen color) 
            : base(pos, dir, size)
        {
            this.color = color;
        }
    }
}
