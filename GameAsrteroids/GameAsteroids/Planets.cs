﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace GameAsteroids
{
    public class Planet : Star
    {
        private static readonly Color DarkPlanetColor = Color.FromArgb(30, 20, 20);
        private Bitmap icon;
       
        public Planet(Point pos, Point dir) 
            : base(pos, dir, Size.Empty)
        {
            icon = Properties.Resources.Planet;
            var p = icon.Palette;
            p.Entries[0] = DarkPlanetColor;
            p.Entries[1] = VeryTransparent;
            icon.Palette = p;
        }

        public override void Draw(Graphics window)
        {
            window.DrawImage(icon, Pos);
        }

    }
}
