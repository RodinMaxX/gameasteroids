﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Collections.Generic;


namespace GameAsteroids
{
    public class Game : IDisposable 
    {
        const string OutOfRangeError = "Значение должно быть больше 0 и меньше 1000!";

        private BufferedGraphics buffer;
        private List<BaseObject> objs;
        private Player[] players;
        private Timer timer;
        private Random rand = new Random();
        private int width, height;
        private Dictionary<BaseObject, object> enemiesCollection;
        private int level;

        public Random Random
        {
            get { return rand; }
        }

        // Ширина и высота игрового поля 
        public int Width
        {
            get
            {
                return width; 
            }
            set
            {
                if (1000 < value || value < 0)
                {
                    throw new ArgumentOutOfRangeException("value", OutOfRangeError );
                }
                width = value; 
            }
        }

        public int Height
        {
            get
            {
                return height;
            }
            set
            {
                if (1000 < value || value < 0)
                {
                    throw new ArgumentOutOfRangeException("value", OutOfRangeError);
                }
                height = value;
            }
        }

        public Graphics Window 
        {
            get { return buffer.Graphics; }
        }

        public virtual void Init(Control form)
        {
            Init(form, false);
        }

        protected void Init(Control form, bool isTitle)
        {
            // предоставляет доступ к главному буферу графического контекста для текущего приложения 
            var context = BufferedGraphicsManager.Current;
            Graphics gr = form.CreateGraphics();
            // Запоминаем размеры формы 
            Width = form.Width;
            Height = form.Height;
            // Связываем буфер в памяти с графическим объектом.
            // для того, чтобы рисовать в буфере
            buffer = context.Allocate(gr, new Rectangle(0, 0, Width, Height));
            enemiesCollection = new Dictionary<BaseObject, object>();
            InitObjects(isTitle);
        }

        public void Draw()
        {
            // Проверяем вывод графики
            buffer.Graphics.Clear(Color.Black);
            foreach (BaseObject obj in objs)
            {
                obj.Draw(Window);
            }

            buffer.Render();
        }

        private IEnumerable<BaseObject> FilterObjects(bool isBullet)
        {
            foreach (BaseObject obj in objs)
            {
                if (obj is ICollision)
                {
                    Bullet bullet = obj as Bullet;
                    if (isBullet)
                    {
                        if (bullet != null) yield return bullet;
                    }
                    else
                    {
                        if (bullet == null) yield return obj;
                    }
                }
            }
        }

        public void Update()
        {
            foreach (BaseObject obj in objs)
            {
                if (obj.IsDead)
                {
                    enemiesCollection.Remove(obj);
                }
                else obj.Update(this);
            }
            objs.RemoveAll(BaseObject.IfDead);
            if (enemiesCollection.Count == 0)
            {
                level++;
                InitEnemies(level);
            }


            foreach (BaseObject first in FilterObjects(false))
            {
                foreach (Bullet bullet in FilterObjects(true))
                {
                    if (first.Collision(bullet))
                    {
                        Log.WriteLine("Столкновение {0} и {1}", first, bullet);
                        bullet.Impact(this, first);
                    }
                }
            }

            if (players != null)
            {
                foreach (SpaceShip player in players)
                {
                    if (player.Score > Math.Pow(player.UsedBonus, 2))
                    {
                        objs.Add(new AidKit(GetPos(1, 1, 1), new Point(-rand.Next(5, 8), rand.Next(-7, 7))));
                        player.UsedBonus++;
                    }
                }
            }
        }

        private Point GetPos(int a, int b, int n)
        {
            return new Point(rand.Next(Width*a/n, Width*b/n), rand.Next(Height));
        }
        
        public void InitObjects(bool isTitle)
        {
            objs = new List<BaseObject>();
           
            for (int i = 0; i < 15; i++)
            {
                objs.Add(new Star (GetPos(0,1,1), new Point(-i, 0), new Size(5, 5)));
            }

            Pen[] colors = new[]
                {
                    Pens.Red,
                    Pens.Cyan,
                    Pens.Yellow,
                };
           
            for (int i = 0; i < 30; i++)
            {
                objs.Add(new ColoredStar (GetPos(0,1,1), new Point(-i, 0), new Size(5, 5), colors [rand.Next(colors.Length)]));
            }

            int PlanetCount = 5; 
            for (int i = 1; i <= PlanetCount; i++)
            {
                int y = Height / (PlanetCount + 2) * i;
                objs.Add(new Planet (new Point (rand.Next(Width), y), new Point(-i, 0)));
            }

            if (isTitle)
            {
                objs.Add(new About(new Point(Width / 2, Height / 2), new Point(3, 3)));
            }
            else
            {
                InitEnemies(level);


                players = new[] 
                {
                    new Player(GetPos(0,1,3), new Point(-10, rand.Next(15) - 7), Color.Yellow, 
                        new Keys[] {Keys.Enter, Keys.Up, Keys.Down, Keys.Left, Keys.Right}), 
                    new Player(GetPos(0,1,3), new Point(-10, rand.Next(15) - 7), Color.Pink, 
                        new Keys[] {Keys.Space, Keys.W, Keys.S, Keys.A, Keys.D}),
                }; 

                objs.AddRange(players);
            }
        }

        private void InitEnemies(int level)
        {
            for (int i = 1; i <= 3+level; i++)
            {
                enemiesCollection.Add(new Asteroid(GetPos(1, 2, 2), new Point(i, -i)), null);
            }
            foreach (BaseObject enemy in enemiesCollection.Keys)
            {
                objs.Add(enemy);
            }
        }

        public void SetTimer()
        {
            timer = new Timer();
            timer.Interval = 100;
            timer.Start();
            timer.Tick += NextUpdate;
        }

        private void NextUpdate(object sender, EventArgs e)
        {
            Draw();
            Update();
        }

        public void Dispose()
        {
            if (timer != null) timer.Stop();
        }

        public void KeyDown(Keys key)
        {
            if (players != null)
            {
                foreach (Player player in players)
                {
                    player.Move(this, key);
                }
            }
        }

        public void Fire(Player player)
        {
            try
            {
                Point bulletPoz = player.Pos + new Size(48, 15);
                Bullet bullet = new Bullet(bulletPoz, new Point(10, rand.Next(-7, 7)), player.Color);
                bullet.Score += player.AddScore;
                objs.Add(bullet);
            }
            catch (GameObjectException ex)
            {
                Log.WriteLine("bullet error: " + ex.Message);
            }

        }
    }



}