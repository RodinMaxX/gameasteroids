﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace GameAsteroids
{
    public class Player : SpaceShip
    {
        private Keys[] ctrlKeys;

        public Player(Point pos, Point dir, Color color, Keys[] ctrlKeys)
            : base(pos, dir, color)
        {
            this.ctrlKeys = ctrlKeys;
            
        }

        public void AddScore(Game main, int n)
        {
            Score += n;
        }

        public void Move (Game main, Keys key)
        {
            if (ctrlKeys != null)
            {
                int index = Array.IndexOf(ctrlKeys, key);
                if (index > 0)
                {
                    const int maxSpeed = 10;
                    dir = SelectDirection(dir, index, maxSpeed);
                    dir = Normalize(dir, maxSpeed);
                }
                else if (index == 0) main.Fire(this);
            }
        }

        private static Point Normalize(Point dir, int maxSpeed)
        {
            int s = (int)Math.Sqrt(dir.X * dir.X + dir.Y * dir.Y);
            if (s > maxSpeed) dir = new Point(dir.X * maxSpeed / s, dir.Y * maxSpeed / s);
            return dir;
        }

        private static Point SelectDirection(Point old,  int index, int speed)
        {
            switch (index)
            {
                case 1: return new Point(old.X, -speed);
                case 2: return new Point(old.X, +speed);
                case 3: return new Point(-speed, old.Y);
                case 4: return new Point(+speed, old.Y);
            }
            return Point.Empty; 
        }
    }
}
