﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;
using System.Collections;

namespace GameAsteroids
{
    public class Asteroid : BaseObject, ICollision 
    {
        private Bitmap icon;
       
        public Asteroid (Point pos, Point dir) 
            : base(pos, dir, Size.Empty)
        {
            icon = Properties.Resources.Asteroid;
            var p = icon.Palette;
            p.Entries[1] = Color.Gray;
            p.Entries[0] = VeryTransparent;
            icon.Palette = p;
        }

        
        public override void Draw(Graphics window)
        {
            window.DrawImage(icon, Pos);
        }

        public override void Update(Game main)
        {
            if (size == Size.Empty) size = icon.Size; 
            BorderUpdate(main);
        }
    }
}
