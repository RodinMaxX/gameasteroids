﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace GameAsteroids
{
    public class Bullet : BaseObject, ICollision
    {
        private static Size defaultSize = new Size(12, 12);
        protected Brush brush;
        private Pen pen;

        public delegate void BulletScoreDelegate(Game main, int score);
        
        public event BulletScoreDelegate Score; 

        public Bullet(Point pos, Point dir, Color color) 
            : base(pos, dir, defaultSize)
        {
            brush = new SolidBrush(color);
            pen = new Pen(color);
            Score += Nothing;
        }

        public override void Draw(Graphics window)
        {
            window.DrawEllipse(pen, Pos.X, Pos.Y, size.Width, size.Height);
            window.FillEllipse(brush, Pos.X + 2, Pos.Y + 2, size.Width - 4, size.Height - 4);
        }

        public override void Update(Game main)
        {
            if (BorderUpdate(main)) Impact(main, null);
        }

        public virtual void Impact(Game main, BaseObject target)
        {
            if ( !(target is AidKit))
            {
                if (target != null && !target.IsDead)
                {
                    Score(main, 1);
                    target.IsDead = true;
                }

                IsDead = true;
            }
        }

        private static void Nothing(Game main, int n)
        {
        }
    }
}
