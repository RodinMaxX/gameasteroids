﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace GameAsteroids
{
    public class About : BaseObject
    {
        private Font font;
        private string text;

        public About(Point pos, Point dir)
            : base(pos, dir, Size.Empty)
        {
            font = new Font("Arial", 16);
            text = "Автор: Максим Родин";
        }

        public override void Draw(Graphics window)
        {
            window.DrawString(text, font, Brushes.Yellow, Pos);
        }

        public override void Update(Game main)
        {
            if (size == Size.Empty)
            {
                size = (main.Window.MeasureString(text, font)).ToSize();
            }
            BorderUpdate(main);
        }

    }
}
