﻿using System;

namespace GameAsteroids
{
    public class GameObjectException : ApplicationException 
    {
        public GameObjectException(string message) 
            : base(message) 
        { 
        }
    }
}
