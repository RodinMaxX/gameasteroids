﻿using System.Drawing;

namespace GameAsteroids
{
    public class SpaceShip : Bullet, ICollision
    {
        private Bitmap icon;

        public int UsedBonus { get; set; }
        public int Energy { get; private set; }
        public new int Score { get; protected set; }
        public Color Color { get; private set; }

        public SpaceShip(Point pos, Point dir, Color color)
            : base(pos, dir, color)
        {
            icon = Properties.Resources.SpaceShip;
            var p = icon.Palette; 
            p.Entries[0] = color;
            p.Entries[1] = VeryTransparent;
            icon.Palette = p;
            Energy = 100;
            brush = new SolidBrush(color);
            this.Color = color;
        }

        public override void Draw(Graphics window)
        {
            window.DrawImage(icon, Pos);
            window.DrawString("Energy:" + Energy, SystemFonts.DefaultFont, brush, Pos.X - 5, Pos.Y - 15);
            window.DrawString("Score:" + Score, SystemFonts.DefaultFont, brush, Pos.X  + 2, Pos.Y + 30);
        }

        public override void Impact(Game main, BaseObject target)
        {
            int oldEnergy = Energy;
            if (target is Asteroid)
            {
                Energy -= 10;
                target.IsDead = true;
            }
            if (target is AidKit && Energy < 100)
            {
                Energy += 50;
                if (Energy > 100) Energy = 100;
                target.IsDead = true;
            }
            if (oldEnergy != Energy)
            {
                string color = Color.Name;
                Log.WriteLine("Уровень энергии игрока {0}: {1} -> {2}", color, oldEnergy, Energy);
            }
        }

        public override void Update(Game main)
        {
            BorderUpdate(main);
        }
    }
}
