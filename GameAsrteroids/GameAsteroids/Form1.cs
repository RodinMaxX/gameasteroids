﻿using System;
using System.Windows.Forms;

namespace GameAsteroids
{
    public partial class Form1 : Form
    {
        private Game main;

        public Form1()
        {
            InitializeComponent();

            main = new Screensaver();
            main.Init(panel1);
            main.SetTimer();
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void StartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            main.Dispose();
            main = new Game();
            main.Init(panel1);
            main.SetTimer();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            main.KeyDown(e.KeyCode);
        }
    }
}
