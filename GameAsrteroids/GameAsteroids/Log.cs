﻿using System;

namespace GameAsteroids
{
    public static class Log
    {
        public static void WriteLine(string str)
        {
            LogReaders(str);
        }

        public static void WriteLine(string format, params object[] args)
        {
            LogReaders(string.Format(format, args));
        }

        public static event Action<string> LogReaders = Nothing;

        private static void Nothing(string str)
        { 
        }
    }
}
