﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace GameAsteroids
{
    public class AidKit : BaseObject, ICollision 
    {
        private static Size defaultSize = new Size(20, 20);

        public AidKit(Point pos, Point dir) 
            : base(pos, dir, defaultSize)
        {
        }
        
        public override void Draw(Graphics window)
        {
            window.FillRectangle(Brushes.Red, Rect);
            window.FillRectangle(Brushes.White, new Rectangle(Pos.X + 8, Pos.Y + 4, 4, 12));
            window.FillRectangle(Brushes.White, new Rectangle(Pos.X + 4, Pos.Y + 8, 12, 4));
        }

        public override void Update(Game main)
        {
            if (BorderUpdate(main) && Pos.X  <= Math.Abs(dir.X))
            {
                    IsDead = true;
            }
        }
    }
}
