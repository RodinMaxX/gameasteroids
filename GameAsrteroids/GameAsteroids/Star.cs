﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace GameAsteroids
{
    public class Star : BaseObject
    {
        protected Pen color;

        public Star(Point pos, Point dir, Size size) 
            : base(pos, dir, size)
        {
            color = Pens.White;
        }

        public override void Draw(Graphics window)
        {
            window.DrawLine(color, Pos.X, Pos.Y, Pos.X + size.Width, Pos.Y + size.Height); 
            window.DrawLine(color, Pos.X + size.Width, Pos.Y, Pos.X, Pos.Y + size.Height);
        }
    
        public override void Update(Game main)
        {
            Pos += (Size)dir;
            if (Pos.X < 0) Pos =  new Point (main.Width, Pos.Y);
        }
    }
}
