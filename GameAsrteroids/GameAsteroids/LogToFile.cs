﻿using System;
using System.IO;

namespace GameAsteroids
{
   public class LogToFile
   {
       private StreamWriter file;

       public LogToFile(string path)
       {
           file = new StreamWriter(path);
       }

       public void WriteLine(string str)
       {
           file.WriteLine(str);
           file.Flush();
       }
   }
}
