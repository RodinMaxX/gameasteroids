﻿using System;
using System.Drawing;

namespace GameAsteroids
{
    public abstract class BaseObject
    {
        protected static readonly Color VeryTransparent = Color.FromArgb(0, 0, 0, 0);

        public Point Pos { get; protected set; }
        protected Point dir;
        protected Size size;
        public bool IsDead { get; set; }
       
        protected BaseObject(Point pos, Point dir, Size size)
        {
            Log.WriteLine("Создан объект: " + this);
            if (pos.X < 0 || pos.Y < 0) throw new GameObjectException("Координаты не могут быть отрицательными");
            if (Math.Pow(dir.X, 2) + Math.Pow (dir.Y, 2) > 1E6) throw new GameObjectException("Скорость слишком большая");
            if (size.Width < 0 || size.Height < 0) throw new GameObjectException("Размер не может быть отрицательным");
            this.Pos = pos;
            this.dir = dir;
            this.size = size;
        }

        public abstract void Draw(Graphics window);

        public abstract void Update(Game main);

        protected bool BorderUpdate(Game main)
        {
            Pos = Pos + (Size)dir;
            Point oldDir = dir;
            if (Pos.X + dir.X < 0) dir.X = Math.Abs(dir.X);
            if (Pos.X + size.Width + dir.X > main.Width) dir.X = -Math.Abs(dir.X);
            if (Pos.Y + dir.Y < 0) dir.Y = Math.Abs(dir.Y);
            if (Pos.Y + size.Height + dir.Y> main.Height) dir.Y = -Math.Abs(dir.Y);
            return (oldDir != dir);
        }

        public bool Collision(ICollision other)
        {
            return (Rect.IntersectsWith(other.Rect));
        }
        
        public Rectangle Rect
        {
            get { return new Rectangle(Pos, size); }
        }

        public static bool IfDead(BaseObject self)
        {
            return self.IsDead;
        }

        public override string ToString()
        {
            return this.GetType().Name;
        }
    }
}
