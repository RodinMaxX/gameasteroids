﻿using System;
using System.Windows.Forms;
using System.IO;

[assembly: System.Reflection.AssemblyCopyright("Родин М.")]

namespace GameAsteroids
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var file = new LogToFile ("GameAsteroids.Log");
            Log.LogReaders += Console.WriteLine;
            Log.LogReaders += file.WriteLine;
            Form form = new Form1();
            Application.Run(form);
         }

    }
}
