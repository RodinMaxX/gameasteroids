﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Drawing;

namespace GameAsteroids
{
    public interface ICollision
    {
        bool Collision(ICollision other);
        Rectangle Rect { get; }
    }
}
